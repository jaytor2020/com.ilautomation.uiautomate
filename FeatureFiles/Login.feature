Feature: Validate login page

  Scenario Outline: Validate login with invalid username and password
    Given User navigates to FreeCRM Application
    When User enters invalid "<UserName>" and "<Password>"
    Then Application throws error message

    Examples: 
      | UserName | Password |
      | sdsfd    | sdfsdf   |
      | wwww     | wwww     |
      | eee      | eee      |
