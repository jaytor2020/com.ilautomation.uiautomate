package runner;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;
import cucumber.api.*;
@RunWith (Cucumber.class)
@CucumberOptions(features="FeatureFiles", 
glue = {"stepDefination"},
//plugin = {"html:target/ExecutionReport"}
plugin = {"json:target/ExecutionReport/cucumber-reports.json"}
)
public class TestRunner {
	
}
