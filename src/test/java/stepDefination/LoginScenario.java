package stepDefination;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import junit.framework.Assert;

public class LoginScenario {
	//System.setProperty("webdriver.gecko.driver","C:\\Jars\\Cucumber_Jars-20190717T124523Z-001\\Cucumber_Jars\\geckodriver.exe");
	
	WebDriver driver;
	
	
	@Given("^User navigates to FreeCRM Application$")
	public void User_navigates_to_FreeCRM_Application() throws Throwable {
		
		//System.setProperty("webdriver.gecko.driver", "C:\\Jars\\Cucumber_Jars-20190717T124523Z-001\\Cucumber_Jars\\geckodriver.exe");
		//System.setProperty("webdriver.gecko.driver", "C:\\Jars\\Cucumber_Jars-20190717T124523Z-001\\Cucumber_Jars\\geckodriver.exe");
		//WebDriverManager.firefoxdriver().setup();
		//driver = new FirefoxDriver();
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://ui.freecrm.com/");
		//driver.get("https://www.google.com/");
		System.out.println("User navigates to FreeCRM Application");
		driver.manage().window().maximize();
	}
	
	@When("^User enters invalid \"([^\"]*)\" and \"([^\"]*)\"$")
	public void User_enters_invalid_UserName_and_Password(String uname, String pwd) throws Throwable {
		System.out.println("User enters invalid UserName and Password");
		driver.findElement(By.name("email")).sendKeys(uname);
		driver.findElement(By.name("password")).sendKeys(pwd);
		driver.findElement(By.xpath("//div[contains(@class,'large blue submit button')]")).click();
		Thread.sleep(2000);
	}

	@Then("^Application throws error message$")
	public void Application_throws_error_message() throws Throwable {
		System.out.println("Application throws error message");
		Assert.assertEquals(true,driver.findElement(By.xpath("//div[contains(@class, 'ui negative message')]")).isDisplayed());
	    //driver.findElement(By.xpath("//div[contains(@class, 'ui negative message']")).isDisplayed();
		driver.quit();
	}
	
}



